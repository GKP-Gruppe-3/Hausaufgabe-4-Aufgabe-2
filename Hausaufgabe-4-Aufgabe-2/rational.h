#ifndef __RATIONAL_H__
#define __RATIONAL_H__

/******************************************************************************
* Aufgabe 2.1 - Struct "rational"
******************************************************************************/

struct rational
{
  long numerator;
  long denominator;
};

typedef struct rational rational_t;

/******************************************************************************
* Aufgabe 2.2 - Funktionsdeklarationen
******************************************************************************/

// Eine rationale Zahl normalisieren.
void rationalNormalize(rational_t *rational);

// Zwei rationale Zahlen addieren.
rational_t rationalAdd(rational_t *left, rational_t *right);

// Zwei rationale Zahlen subtrahieren.
rational_t rationalSubtract(rational_t *left, rational_t *right);

// Zwei rationale Zahlen multiplizieren.
rational_t rationalMultiply(rational_t *left, rational_t *right);

// Zwei rationale Zahlen dividieren.
rational_t rationalDivide(rational_t *left, rational_t *right);

// Eine rationale Zahl als "double"-Wert liefern.
double rationalToDouble(rational_t *rational);

#endif
