#include "stdafx.h"

#include "rational.h"
#include "util.h"

#include <stdio.h>

/******************************************************************************
* Aufgabe 2.4 - Ein-/Ausgabe: Implementation
******************************************************************************/

// Eine rationale Zahl von der Konsole lesen.
rational_t read_rational()
{
  rational_t result;

  result.numerator = longGetFromConsole("  Nenner:  ");
  result.denominator = longGetFromConsole("  Zaehler: ");

  rationalNormalize(&result);

  return result;
}

// Eine rationale Zahl auf der Konsole ausgeben.
void print_rational(rational_t rational)
{
  printf("%d/%d", rational.numerator, rational.denominator);
}
