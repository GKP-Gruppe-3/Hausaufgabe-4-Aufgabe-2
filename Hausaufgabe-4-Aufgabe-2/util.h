#ifndef __UTIL_H__
#define __UTIL_H__

// Die ie gesamte Ausgabe bereinigen.
void clearScreen();

// Die Ein-/Ausgabe auf der Konsole pausieren,
// und anschließend die gesamte Ausgabe bereinigen.
void pause();

// Einen "int"-Wert von der Konsole lesen.
int intGetFromConsole(char const* const message, ...);

// Einen "long"-Wert von der Konsole lesen.
long longGetFromConsole(char const* const message, ...);

// Einen "double"-Wert von der Konsole lesen.
double doubleGetFromConsole(char const* const message, ...);

#endif
