# Hausaufgabe 4

## Aufgabe 2: Rationale Zahlen (21 Punkte)

Für die Teilaufgaben i) bis iii) ist ein eigenes Modul rational bestehend aus Header
(rational.h) und Implementierungsdatei (rational.(c|cpp)) anzulegen.

### i) Verbundstruktur erstellen (1 Punkte)

Definieren Sie mittels struct eine Verbund-Datenstruktur rational zur Repräsentation
von rationalen Zahlen in Bruchform. Zähler und Nenner sollen darin als Komponenten
des Typs long repräsentiert werden und zwar mit den Komponentennamen numerator
und denominator.
Anmerkung: Denken Sie dabei an die Verwendung von typedef zur Definition eines
eigenen Datentyps in C.

### ii) Funktionen definieren (2 Punkte)

Deklarieren Sie die Signaturen der Funktionen für die vier Grundrechenoperationen Addition,
Subtraktion, Multiplikation und Division. Die Parameter und Rückgabewerte dieser
Funktionen sollen jeweils vom zuvor vereinbarten struct-Typ rational sein.

### iii) Funktionen implementieren (8 Punkte)

Implementieren Sie die in Teilaufgabe ii) deklarierten Funktionen. Alle Berechnung sollen
durchgängig im rationalen Bereich (sprich Bruchrechnung) durchgeführt werden.
Sorgen Sie dafür, dass die Funktionen stets wie folgt normalisierte Ergebnisse zurückgeben:
* Das Resultat soll gekürzt sein (Zähler und Nenner müssen teilerfremd sein).
* Der Nenner soll kein negatives Vorzeichen haben – wenn die rationale Zahl insgesamt
negativ ist (weder positiv noch Null), soll stets der Zähler das negative
Vorzeichen tragen.
Hinweis: Implementieren Sie dazu den Euklidschen Algorithmus in einer geeigneten Version
zur Berechnung des größten gemeinsamen Teilers (greatest common divisor/ggT)
zweier Ganzzahlen als Hilfsfunktion. Achten Sie darauf, inwieweit die Funktion mit negativen
Zahlen zurechtkommt.

### iv) Eingabe und Ausgabe (4 Punkte)

Erstellen Sie im anzulegenden Quelldateienpaar rational_IO.h / rational_IO.(c|cpp)
je eine Funktion zur Ein- und Ausgabe einer rationalen Zahl über die Konsole:
* Die Eingabefunktion heiße read_rational und die Ausgabefunktion print_rational.
* Erstere soll keinen Parameter, aber ein Resultat vom Typ rational haben.
* Letztere hat einen Parameter des Typs rational, aber keinen Rückgabewert.
* Die Eingaberoutine könnte zum Beispiel Zähler und Nenner einzeln vom Benutzer abfragen, aber auch anders vorgehen. Ihr Resultat soll normalisiert sein.
* Die Ausgaberoutine soll die jeweilige rationale Zahl ohne weiteren umgebenden
Text in der Form <zaehler>/<nenner> ausgeben, also z. B. als 7/13.

### v) Testprogramm (6 Punkte)

Schreiben Sie ein interaktives Testprogramm, um die zuvor implementierten Funktionen
zu testen. Dabei soll der Nutzer wiederholt Brüche eingeben können, die dann als
Operanden für die Funktionen dienen.
